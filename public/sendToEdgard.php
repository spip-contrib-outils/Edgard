<?php

require dirname(__DIR__).'/vendor/autoload.php';

if (file_exists(dirname(__DIR__).'/config/edgard.php')) {
    $config = include dirname(__DIR__).'/config/edgard.php';
} else {
    throw new \Exception("You have to copy app.dist.php to edgard.php and edit it’s config.");
}

array_shift($argv);
$content = implode(" ", $argv);
if (!$content) {
    $content = file_get_contents('php://stdin');
}
if ($content) {
    $client = new Hoa\Websocket\Client(
        new Hoa\Socket\Client($config->ws)
    );
    $client->setHost('localhost');
    $client->connect();
    $client->send($content);
}