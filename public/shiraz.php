<?php

require dirname(__DIR__).'/vendor/autoload.php';

if (file_exists(dirname(__DIR__).'/config/shiraz.php')) {
    $config = include dirname(__DIR__).'/config/shiraz.php';
} else {
    throw new \Exception("You have to copy app.dist.php to shiraz.php and edit it’s config.");
}

$shiraz = new Edgard\Bot($config);