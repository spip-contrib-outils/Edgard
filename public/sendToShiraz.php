<?php

require dirname(__DIR__).'/vendor/autoload.php';

if (file_exists(dirname(__DIR__).'/config/shiraz.php')) {
    $config = include dirname(__DIR__).'/config/shiraz.php';
} else {
    throw new \Exception("You have to copy app.dist.php to shiraz.php and edit it’s config.");
}

array_shift($argv);
$content = implode(" ", $argv);
if (!$content) {
    $content = file_get_contents('php://stdin');
}
if ($content) {
    $client = new Hoa\Websocket\Client(
        new Hoa\Socket\Client($config->ws)
    );
    $client->setHost('localhost');
    $client->connect();
    $client->send($content);
}