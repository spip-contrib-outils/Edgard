<?php

require dirname(__DIR__).'/vendor/autoload.php';

if (file_exists(dirname(__DIR__).'/config/edgard.php')) {
    $config = include dirname(__DIR__).'/config/edgard.php';
} else {
    throw new \Exception("You have to copy app.dist.php to edgard.php and edit it’s config.");
}

$edgard = new Edgard\WikiBot($config);