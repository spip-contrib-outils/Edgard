<?php

return new Edgard\Config([
    #'irc' => 'irc://irc.libera.chat',
    #'ws' => 'ws://127.0.0.1:8889',
    #'channel' => '#spip',
    #'nickname' => 'Edgard',
    #'password' => 'blabla',

    # For Wiki Bot
    'wiki_edgard' => '~/edgard.spip.net/tmp/edgard.txt',
    'wiki_faq' => '~/edgard.spip.net/tmp/faq.txt',
    #'ping_edgard' => 'https://edgard.spip.net/spip.php?page=edgard&var_mode=calcul',
    #'ping_faq' => 'https://edgard.spip.net/spip.php?page=mafaq&var_mode=calcul',
]);