Edgard est le bot de #spip

# Installation

    git clone https://git.spip.net/spip-contrib-outils/Edgard.git
    cd Edgard
    composer install

# Configuration

    cd config
    cp app.dist.php app.php
    # adapter la config

# Exécution

    # serveur (se connecte, reste connecté et à l’écoute...)
    php public/edgard.php

    # envoyer des messages
    echo "Mon beau message" | php public/send.php
    php public/sendToEdgard.php "Mon beau message"




