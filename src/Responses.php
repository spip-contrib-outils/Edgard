<?php

namespace Edgard;

class Responses {

    protected $responses = [
        'hello' => [
            "Bonjour à tout·e·s !",
            "Salutations",
            "Welcome back :)",
            "Bonjour ! Vous avez des noisettes ?",
            "%me% est de retour !",
            "C’est une belle journée ! Salut :)",
        ],
        'mentionned' => [
            "Je ne suis qu’un bot %nick% !",
            "Yep ?",
            "Hé, un bot ne sait rien !",
            "Un peu de silence %nick%...",
            "Je n’ai pas bien compris, tu peux répéter ?",
            "Tu désirais ?",
        ],
        'rechargerWiki' => [
            "Je recharge le wiki, très bien… Faut toujours bosser ici !",
            "Oui oui, le wiki...",
            "Encore ?",
            "On peut jamais être tranquille !",
            "Alors %nick% ? On écrit encore des blagues ?",
            "Merci %nick% ! Je vais apprendre de nouveaux mots !",
        ],
    ];


    public function __construct() {}


    public function get(string $key) {
        if (!isset($this->responses[$key])) {
            throw new \RuntimeException("$key is not part of Responses key list");
        }
        return $this->random($this->responses[$key]);
    }

    public function getHello() {
        return $this->get('hello');
    }

    public function getMentionned() {
        return $this->get('mentionned');
    }

    public function getRechargerWiki() {
        return $this->get('rechargerWiki');
    }

    protected function random(array $list) {
        $i = random_int(0, count($list) - 1);
        return $list[$i];
    }

}