<?php

namespace Edgard;

class Config {

    protected $irc = 'irc://irc.libera.chat';
    protected $ws = 'ws://127.0.0.1:8889';
    protected $channel = '#spip';
    protected $nickname = 'Edgard';
    protected $password = null;
    /** @param string full path to wiki edgard file */
    protected $wiki_edgard = '/tmp/edgard.txt';
    /** @param string full path to wiki faq file */
    protected $wiki_faq = '/tmp/faq.txt';

    protected $ping_edgard = 'https://edgard.spip.net/spip.php?page=edgard&var_mode=calcul';
    protected $ping_faq = 'https://edgard.spip.net/spip.php?page=mafaq&var_mode=calcul';

    public function __construct(array $config) {
        foreach ($config as $name => $value) {
            if (!property_exists($this, $name)) {
                throw new \Exception("Config $name unknown");
            }
            $this->$name = $value;
        }
    }

    public function __get($name) {
        if (property_exists($this, $name)) {
            return $this->$name;
        }
        throw new \Exception("Property $name unknown");
    }

}