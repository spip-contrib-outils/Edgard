<?php

namespace Edgard;

use Hoa\Socket\Connection\Group;
use Hoa\Irc\Client as IrcClient;
use Hoa\Socket\Client as SocketClient;
use Hoa\Socket\Server as SocketServer;
use Hoa\Websocket\Server as WsServer;
use Hoa\Event\Bucket;

class Bot {

    protected $config;
    protected $client;
    protected $client_notifications;
    protected $server;
    protected $group;
    protected $wiki;
    protected $responses;

    public function __construct(Config $config) {
        $this->config = $config;

        $this->group  = new Group();
        $this->client = new IrcClient(new SocketClient($config->irc));
        $this->server = new WsServer(new SocketServer($config->ws));
        $this->group[] = $this->server;
        $this->group[] = $this->client;

        // nos réponses aléatoires
        $this->responses = new Responses();

        $this->startClientEvents();
        $this->startServerEvents();

        $this->group->run();
    }

    protected function startClientEvents() {

        $this->client->on('open', function (Bucket $bucket) {
            $bucket->getSource()->join($this->config->nickname, $this->config->channel, $this->config->password);
            return;
        });

        $this->client->on('join', function (Bucket $bucket) {
            $this->say($bucket, $this->responses->getHello());
            return;
        });
    }

    protected function startServerEvents() {
        $this->server->on('message', function (Bucket $bucket) {
            $message = $bucket->getData()['message'];
            $this->client->say($message);
            return;
        });
    }

    /**
     * Say, qui remplace les placeholders...
     */
    protected function say(Bucket $bucket, string $message) {
        $message = $this->replacePlaceholders($bucket, $message);
        $bucket->getSource()->say($message);
    }

    /**
     * Replace placeholders (%nick%, %me% ...) in message
     *
     * @param Bucket $bucket
     * @param string $message
     * @return string
     */
    protected function replacePlaceholders(Bucket $bucket, string $message) {
        $myNick = $bucket->getSource()->getConnection()->getCurrentNode()->getUsername();
        $data = $bucket->getData();
        $replaces = [];
        $replaces['%me%'] = $myNick;
        $replaces['%nick%'] = $data['from']['nick'] ?? '';
        return str_replace(array_keys($replaces), $replaces, $message);
    }

    public function getClient() {
        return $this->client;
    }

}

