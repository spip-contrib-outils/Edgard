<?php

namespace Edgard;

class WikiResponse {

    protected $regexp;
    protected $response;
    protected $file;
    protected $line_no;

    public function __construct(string $regexp, string $response, string $file, int $line_no) {
        $this->regexp = $regexp;
        $this->response = $response;
        $this->file = $file;
        $this->line_no = $line_no;
    }

    public function getRegexp() {
        return $this->regexp;
    }

    public function getResponse() {
        return $this->response;
    }

    public function getFile() {
        return $this->file;
    }

    public function getLineNo() {
        return $this->line_no;
    }
}