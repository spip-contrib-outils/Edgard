<?php

namespace Edgard;

use Hoa\Socket\Connection\Group;
use Hoa\Irc\Client as IrcClient;
use Hoa\Socket\Client as SocketClient;
use Hoa\Socket\Server as SocketServer;
use Hoa\Websocket\Server as WsServer;
use Hoa\Event\Bucket;

class WikiBot extends Bot {

    protected $wiki;

    public function __construct(Config $config) {

        // notre super Wiki de regexp...
        $this->wiki = new Wiki($config);
        $this->wiki->load();

        parent::__construct($config);
    }

    protected function startClientEvents() {
        parent::startClientEvents();

        $this->client->on('message', function (Bucket $bucket) {
            if ($this->wikiResponse($bucket)) {
                return;
            }
            return;
        });

        $this->client->on('mention', function (Bucket $bucket) {
            if ($this->actionReloadWiki($bucket)) {
                // nothing more to do...
                return;
            }
            if ($this->wikiResponse($bucket)) {
                return;
            }
            $this->say($bucket, $this->responses->getMentionned());
            return;
        });
    }

    /** Si le wiki trouve une réponse pour ce message, l’envoyer */
    protected function wikiResponse(Bucket $bucket): bool {
        $message = $bucket->getData()['message'];
        if ($response = $this->wiki->getResponseFor($message)) {
            $this->say($bucket, $response);
            return true;
        }
        return false;
    }

    /** Regarcher le wiki si le message contient 'wiki' */
    protected function actionReloadWiki(Bucket $bucket): bool {
        $message = $bucket->getData()['message'];
        if (false !== stripos($message, 'wiki')) {
            $this->say($bucket, $this->responses->getRechargerWiki());
            $n = $this->wiki->reload();
            $this->say($bucket, "Wiki à jour... Je connais $n formules :)");
            $errors = $this->wiki->getErrors();
            if ($errors) {
                $this->say($bucket, count($errors) . " regexp en erreurs :( dont : ");
                $erreur = reset($errors);
                $this->say($bucket, "Wiki " . $erreur->getFile() . ':' . $erreur->getLineNo() . ' sur ' . $erreur->getRegexp());
            }
            return true;
        }
        return false;
    }
}

