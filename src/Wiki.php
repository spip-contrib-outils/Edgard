<?php

namespace Edgard;

class Wiki {

    protected $config;
    protected $wikiResponses = [];
    protected $errors = [];

    public function __construct(Config $config) {
        $this->config = $config;
    }

    public function reload() {
        if ($this->config->ping_edgard) {
            file_get_contents($this->config->ping_edgard);
        }
        if ($this->config->ping_faq) {
            file_get_contents($this->config->ping_faq);
        }
        return $this->load();
    }

    public function load() {
        $this->wikiResponses = [];
        $this->errors = [];
        $this->loadFile($this->config->wiki_faq);
        $this->loadFile($this->config->wiki_edgard);
        return count($this->wikiResponses);
    }

    public function loadFile($file) {
        if (!$file) {
            return false;
        }
        if (!file_exists($file)) {
            return false;
        }
        $content = file_get_contents($file);
        if (!$content) {
            return false;
        }
        $filename = basename($file);
        $lines = explode("\n", $content);

        foreach ($lines as $line_no => $line) {
            if ($line = $this->isValidLine($line)) {
                $this->readLine($line, $filename, $line_no + 1);
            }
        }
        return true;
    }

    public function getResponseFor(string $message) {
        foreach ($this->wikiResponses as $wikiResponse) {
            if (preg_match($wikiResponse->getRegexp(), $message, $m)) {
                $message = $wikiResponse->getResponse();
                unset($m[0]);
                if ($m) {
                    $keys = array_map(function($i) { return "\$$i"; },  array_keys($m));
                    $message = str_replace($keys, $m, $message);
                }
                return $message;
            }
        }
        return null;
    }

    public function getErrors() {
        return $this->errors;
    }

    /**
     * Read wiki line and add it to autoResponse list (or error list if regexp parse failed)
     *
     * Line as "regexp;response"
     * 
     * @param string $line
     * @param string $filename
     * @param int $line_no_human
     * @return void
     */
    protected function readLine($line, $filename, $line_no_human) {
        list ($regexp, $response) = explode(';', $line, 2);
        $regexp = '~' . str_replace('~', '\~', $regexp) . '~i'; // edgard V1 : ~iAD
        $wikiResponse = new WikiResponse($regexp, $response, $filename, $line_no_human);
        if (@preg_match($regexp, '') !== false) {
            $this->wikiResponses[] = $wikiResponse;
        } else {
            // regex was invalid
            $this->errors[] = $wikiResponse;
        }
    }

    protected function isValidLine(string $line): ?string {
        if (!$line) {
            return null;
        }
        $line = trim($line);
        if (!$line) {
            return null;
        }
        if (false === strpos($line, ';')) {
            return null;
        }
        return $line;
    }

}